.PHONY: all build test

all: build

build: meson-build-release
	meson compile -C meson-build-release

test: meson-build-release
	meson test -C meson-build-release

meson-build-release:
	meson setup meson-build-release --buildtype release