#pragma once

#include <cstdint>
#include <cstddef>
#include <unistd.h>
#include <sys/epoll.h>
#include <array>
#include "EpollEntry.hpp"
#include "error_util.hpp"

class EpollEventLoop {
public:
    static constexpr size_t MAX_EPOLL_EVENTS = 64;
private:
    int fd_;
    bool should_stop_ = false;

public:
    EpollEventLoop() : fd_{CHECK_ERROR(epoll_create1(0))} {}

    ~EpollEventLoop() {
        close(fd_);
    }

    // we have a file descriptor, prevent copy & move
    EpollEventLoop(const EpollEventLoop&) = delete;
    EpollEventLoop& operator=(const EpollEventLoop&) = delete;
    EpollEventLoop(EpollEventLoop&&) = delete;
    EpollEventLoop& operator=(EpollEventLoop&&) = delete;

    /** Add an epoll entry to this epoll instance. */
    void add(EpollEntry& e) const {
        epoll_event ev{};
        ev.events = e.events();
        ev.data.ptr = &e;
        CHECK_ERROR(epoll_ctl(fd_, EPOLL_CTL_ADD, e.fd(), &ev));
    }

    /**
     * Remove an epoll entry from this epoll instance. Invokes the `on_remove()`
     * callback of the entry.
     */
    void remove(EpollEntry& e) const {
        epoll_event ev{};
        ev.events = 0;
        ev.data.ptr = &e;
        CHECK_ERROR(epoll_ctl(fd_, EPOLL_CTL_DEL, e.fd(), &ev));
        e.on_remove();
    }

    /** Runs the event loop until `.stop()` is called. */
    void run() {
        should_stop_ = false;
        while (!should_stop_) {
            wait_and_handle_events();
        }
    }

    /** Stops the event loop after all currently pending events are processed. */
    void stop() {
        should_stop_ = true;
    }

private:
    void wait_and_handle_events() const {
        std::array<epoll_event, MAX_EPOLL_EVENTS> events{};
        int n = CHECK_ERROR_EINTR(epoll_wait(fd_, events.data(), events.size(), -1));
        if (n == -1) {
            return; // EINTR, received a signal
        }

        for (int i = 0; i < n; i++) {
            auto* e = static_cast<EpollEntry*>(events[i].data.ptr);
            if (!e->handle_event(events[i].events)) {
                remove(*e);
            }
        }
    }
};