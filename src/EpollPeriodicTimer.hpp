#pragma once

#include <cstdint>
#include <cstring>
#include <unistd.h>
#include <sys/timerfd.h>
#include <chrono>
#include <type_traits>
#include "EpollEntry.hpp"
#include "error_util.hpp"

template<typename Callback>
class EpollPeriodicTimer : public EpollEntryRAII {
private:
    Callback cb_;

public:
    /**
     * @param timeout - timer period
     * @param cb - callback, invoked whenever the timer expires; it is invoked either
     * as `cb(expiration_count)` or `cb()`, whichever is supported
     */
    EpollPeriodicTimer(std::chrono::nanoseconds timeout, Callback cb)
            : EpollEntryRAII(CHECK_ERROR(timerfd_create(CLOCK_MONOTONIC, TFD_NONBLOCK)), EPOLLIN),
              cb_{cb} {
        using namespace std::chrono;

        // set up the timerfd interval
        itimerspec its{};
        auto sec_part = duration_cast<seconds>(timeout);
        its.it_interval.tv_sec = sec_part.count();
        its.it_interval.tv_nsec = nanoseconds(timeout - sec_part).count();
        its.it_value = its.it_interval;
        CHECK_ERROR(timerfd_settime(fd_, 0, &its, nullptr));
    }

    bool handle_event(uint32_t events) override {
        if ((events & EPOLLERR) || (events & EPOLLHUP) || !(events & EPOLLIN)) {
            return false;
        } else {
            uint64_t expiration_count;
            check_error("timerfd read", read(fd_, &expiration_count, 8));
            // support lambda without arguments
            if constexpr (std::is_invocable_v<Callback, uint64_t>) {
                cb_(expiration_count);
            } else {
                cb_();
            }
            return true;
        }
    }
};