#include "EpollEventLoop.hpp"
#include "EpollStdInLine.hpp"
#include "EpollPeriodicTimer.hpp"
#include <iostream>

int main() {
    EpollEventLoop loop{};

    using namespace std::chrono_literals;
    EpollPeriodicTimer tim1(1000ms, [&] { std::cout << "timer: 4" << std::endl; });
    EpollPeriodicTimer tim2(1500ms, [&] { std::cout << "timer: 5" << std::endl; });
    EpollStdInLine sin([&](const std::string& line) {
        std::cout << "stdin: " << line << std::endl;
    });

    loop.add(sin);
    loop.add(tim1);
    loop.add(tim2);

    loop.run();

    loop.remove(tim2);
    loop.remove(tim1);
    loop.remove(sin);

    return 0;
}
