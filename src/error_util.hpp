#pragma once

#include <system_error>
#include <cerrno>
#include <string>

static inline std::system_error system_error_from_errno(const std::string& fn) {
    return {std::error_code(errno, std::system_category()), fn};
}

template<typename T>
static inline T check_error(const std::string& fn, T return_value, bool allow_eintr = false) {
    if (return_value == -1 && (!allow_eintr || errno != EINTR)) {
        throw system_error_from_errno(fn);
    }
    return return_value;
}

#define CHECK_ERROR(fn) check_error(#fn, fn)
#define CHECK_ERROR_EINTR(fn) check_error(#fn, fn, true)