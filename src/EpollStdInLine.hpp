#pragma once

#include <iostream>
#include "EpollEntry.hpp"

template<typename Callback>
class EpollStdInLine : public EpollEntry {
private:
    Callback cb_;

public:
    explicit EpollStdInLine(Callback cb) : EpollEntry(STDIN_FILENO, EPOLLIN), cb_{cb} {}

    bool handle_event(uint32_t events) override {
        if ((events & EPOLLERR) || (events & EPOLLHUP) || !(events & EPOLLIN)) {
            return false;
        } else {
            std::string line;
            std::getline(std::cin, line);
            cb_(line);
            return true;
        }
    }
};