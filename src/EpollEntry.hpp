#pragma once

#include <cstdint>
#include <cassert>
#include <unistd.h>
#include <sys/epoll.h>
#include <utility>

class EpollEntry {
protected:
private:
    /** Events this epoll entry reacts to. */
    uint32_t events_;
protected:
    /** The file descriptor this epoll entry is polling on. */
    int fd_;

public:
    EpollEntry(int fd, uint32_t events) : events_{events}, fd_{fd} {
        assert(fd >= 0);
    }

    virtual ~EpollEntry() = default;

    /** This callback is called by `EpollEventLoop` when the entry is removed. */
    virtual void on_remove() {}

    /**
     * Invoked when the epoll entry is signalled.
     * Returns false if this entry should be removed from the invoking `EpollInstance`.
     */
    virtual bool handle_event(uint32_t events) = 0;

    [[nodiscard]] int fd() const {
        assert(fd_ >= 0);
        return fd_;
    }

    [[nodiscard]] uint32_t events() const {
        return events_;
    }
};

/** A variant of `EpollEntry` which closes its file descriptor when destructed. */
class EpollEntryRAII : public EpollEntry {
public:
    EpollEntryRAII(int fd, uint32_t events) : EpollEntry{fd, events} {}

    ~EpollEntryRAII() override {
        // only close the FD if we haven't been moved from
        if (fd_ != -1) {
            close(fd_);
        }
    }

    // we're destructing our FD – prevent copy
    EpollEntryRAII(const EpollEntryRAII&) = delete;
    EpollEntryRAII& operator=(const EpollEntryRAII&) = delete;

    // allow move
    EpollEntryRAII(EpollEntryRAII&& e) noexcept: EpollEntry(e) {
        e.fd_ = -1; // make the moved-from FD invalid
    }

    EpollEntryRAII& operator=(EpollEntryRAII&& e) noexcept {
        if (this != &e) {
            // make the moved-from FD invalid
            fd_ = std::exchange(e.fd_, -1);
        }
        return *this;
    }
};